# Разработчик PHP. Тестовое задание от FEIP #

Предположим, что перед нами стоит задача обработать HTTP запрос или ответ от стороннего API, содержащий данные в формате JSON. 
Нужно написать санитайзер — класс, который провалидирует и преобразует переданные данные в требуемый формат.

Требования:

 - Язык PHP 7.1+ без сторонних библиотек (кроме библиотек для тестирования)
 - Поддержка строк, целых чисел, чисел с плавающей точкой и российских федеральных номеров телефонов
 - Поддержка массивов произвольной длины с элементами одного определённого типа
 - Поддержка структур — ассоциативных массивов произвольной вложенности с заранее известными ключами
 - Генерация ошибок для некорректных значений, возвращение списка ошибок на клиент
 - Возможность расширения путём добавления новых типов параметров
 - Тесты

Примеры:

1. Из объекта '{"foo": "123", "bar": "asd", "baz": "8 (950) 288-56-23"}' при указанных программистом типах "целое число", "строка" и "номер телефона" соответственно должен получиться объект с тремя полями: целочисленным foo = 123, строковым bar = "asd" и строковым "baz" = "79502885623".

2. При указании для строки "123абв" типа "целое число" должна быть сгенерирована ошибка.

3. При указании для строки "260557" типа "номер телефона" должна быть сгенерирована ошибка.

https://docs.google.com/forms/d/e/1FAIpQLSceagaihDrbc4XbkYtFhSc81GRNHWVzpLW9azBSC9mOchf3Og/viewform?vc=0&c=0&w=1

https://vladivostok.hh.ru/employer/2759120


# Решение #

Для запуска локального сервера - необходиммо запустить Docker - в консоли выполнить **[./run.sh](https://bitbucket.org/xtetis/php-test-feip.co/src/master/run.sh "./run.sh")** или **sudo -S docker-compose up**

Можно также использовать свой сервер. Решение находится в папке [./web/html ](https://bitbucket.org/xtetis/php-test-feip.co/src/master/web/html/ "./web/html ")

Сам класс, ** [jsonValidator.php](https://bitbucket.org/xtetis/php-test-feip.co/src/master/web/html/inc/jsonValidator.php "jsonValidator.php")** находится в https://bitbucket.org/xtetis/php-test-feip.co/src/master/web/html/inc/jsonValidator.php 

**Скачать репозиторий** можно здесь https://bitbucket.org/xtetis/php-test-feip.co/downloads/

Для тестирования использовал примеры из папки [./web/html/examples/1/](https://bitbucket.org/xtetis/php-test-feip.co/src/master/web/html/examples/1/ "./web/html/examples/1/")

JSON для отправки - [send.json](https://bitbucket.org/xtetis/php-test-feip.co/src/master/web/html/examples/1/send.json "send.json")
Правила валидации - [rules.php](https://bitbucket.org/xtetis/php-test-feip.co/src/master/web/html/examples/1/rules.php "rules.php")

Тесты - сделал в представленных примерах. О Unit тестах речь в задании не шла.