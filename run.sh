# run.sh
clear

# Меняем права
echo -e "\e[43m*************************************\e[0m"
echo -e "\e[92mМеняем права\e[0m"
echo -e "\e[43m*************************************\e[0m"
sudo -S chmod 777 ./web/log -Rv
sudo -S chmod 777 ./web/html/examples -Rv
sudo -S chmod 777 ./web/html/assets -Rv
echo ""
echo ""


echo -e "\e[43m*************************************\e[0m"
echo -e "\e[92mПерезапускаем сервис докера\e[0m"
echo -e "\e[43m*************************************\e[0m"
sudo service docker restart
echo ""
echo ""


# Запускаем наш локальный сервер
echo -e "\e[43m*************************************\e[0m"
echo -e "\e[92mЗапускаем наш локальный сервер docker-compose\e[0m"
echo -e "\e[43m*************************************\e[0m"
sudo -S docker-compose up
