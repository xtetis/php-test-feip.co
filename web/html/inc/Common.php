<?php


class Common
{

  

    /**
     * Функция проверяет является ли строка корректным JSON
     */
    public static function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Функция конфертирует массив в JSON с оговоркой,
     * если предопределена константа JSON_UNESCAPED_UNICODE
     */
    public static function arrayToJson($array)
    {
        if (defined('JSON_UNESCAPED_UNICODE') && defined('JSON_PRETTY_PRINT'))
        {
            $ret = json_encode($array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        else
        {
            $ret = json_encode($array);
        }

        return $ret;
    }
}
