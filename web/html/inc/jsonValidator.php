<?php

/**
 * Класс для валидации данных
 *
 * для добавления новых правил валидации необходимо добавлять новые функции по типу
 * validateActionRequired()- где "Required" - ключевое слово, которое используется
 * в правилах валидации
 *
 */

class jsonValidator
{

    /**
     * @var mixed
     * [
     *      [
     *          'fields' = ['field1','field20,...]
     *          'action' = 'string'
     *          'params' = [
     *              'max' => 1,
     *              'message' => '',
     *          ],
     *      ],
     *      ...
     * ]
     */
    public $rules = [];

    /**
     * Если поля являются предопределенными полями, они заменяются на соответствующие названия
     *
     * @var array
     */
    protected $labels = [
        'snils' => 'СНИЛС',
    ];

    /**
     * Сообщения об ошибках при определенных проверках
     *
     * @var array
     */
    protected $validateErrorMessages = [
        'Required'        => 'Поле "%field%" не задано',
        'String'          => 'Поле "%field%" не является строкой',
        'StringMinLength' => 'Минимальная длина поля "%field%" составляет %min% символов',
        'StringMaxLength' => 'Максимальная длина поля "%field%" составляет %max% символов',
        'RegexTest'       => 'Поле "%field%" имеет некорректный формат',
        'InvalidRegex'    => 'Для поля "%field%" указано некорректное регулярное выражение',
        'Integer'         => 'Поле "%field%" не является целым числом',
        'Float'           => 'Поле "%field%" не является числом с плавающем запятой',
        'Phone'           => 'Поле "%field%" не является корректным номером телефона',
    ];

    /**
     * @var mixed
     */
    public $data;

    /**
     * @var mixed
     */
    protected $current_rule;

    /**
     * Хранится текущий путь в массиве согласно правилам проверки
     * @var mixed
     */
    protected $current_path;
    /**
     * Хранится текущее значение пути
     * @var mixed
     */
    protected $currentFieldValue;

    /**
     * @var mixed
     */
    protected $current_action;
    /**
     * @var mixed
     */
    protected $currentErrorMessage;
    /**
     * @var mixed
     */
    protected $currentFieldLabel;

    /**
     * @var mixed
     */
    public $errors = [];

    /**
     * @param  $data
     * @param  $rules
     * @return mixed
     */
    public function __construct(
        $json,
        $rules
    )
    {
        $this->errors['result'] = true;

        if (!self::isJson($json))
        {
            $this->errors['result'] = false;
            $this->errors['common'] = 'Правила валидации некорректные. Не заданы параметры.';
        }
        else
        {
            $this->data  = json_decode($json, true);
            $this->rules = $rules;
        }
    }

    /**
     * Функция проверяет является ли строка корректным JSON
     */
    public static function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param $field
     * @param $name
     */
    public function setLabel(
        $field,
        $name
    )
    {
        $this->labels[$field] = $name;
    }

    /**
     * @return mixed
     */
    public function checkRules()
    {
        if ($this->errors['result'])
        {
            foreach ($this->rules as $rule)
            {
                if (
                    (!isset($rule['fields'])) ||
                    (!isset($rule['action'])) ||
                    (!isset($rule['params'])) ||
                    (!is_array($rule['fields'])) ||
                    (!count($rule['fields'])) ||
                    (!is_array($rule['params'])) ||
                    (!strlen($rule['action']))

                )
                {
                    $this->errors['result'] = false;
                    $this->errors['common'] = 'Правила валидации некорректные. Не заданы параметры.';
                    break;
                }
            }
        }

        if ($this->errors['result'])
        {
            foreach ($this->rules as $rule)
            {
                if (!method_exists($this, 'validateAction' . $rule['action']))
                {
                    $this->errors['result'] = false;
                    $this->errors['common'] = 'Правило валидации ' . $rule['action'] . ' не существует';
                    break;
                }
            }
        }

        return $this->errors['result'];
    }

    /**
     * Проверят входящие данные
     *
     * @return mixed
     */
    public function checkData()
    {
        if ($this->errors['result'])
        {
            if (
                (!is_array($this->data)) ||
                (!count($this->data))
            )
            {
                $this->errors['result'] = false;
                $this->errors['common'] = 'Некорректные входные данные';
            }
        }

        return $this->errors['result'];
    }

    /**
     * @param  $data
     * @return mixed
     */
    public function validate()
    {
        if ($this->errors['result'])
        {
            if ($this->checkRules() && $this->checkData())
            {

                foreach ($this->rules as $rule)
                {
                    $this->current_rule   = $rule;
                    $this->current_action = $rule['action'];
                    $rulesPaths           = self::getRulesAllPaths($rule['fields']);
                    foreach ($rulesPaths as $path)
                    {
                        $this->current_path = $path;
                        if (!isset($this->errors['fields'][$this->current_path]))
                        {
                            $this->currentFieldValue = $this->getFieldValueByPath();
                            call_user_func([$this, 'validateAction' . $rule['action']]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Генерирует сообщение об ошибке
     * В случае необходимости - кастомное
     *
     * @return mixed
     */
    public function makeValidateErrorMessage($messageType = '')
    {
        $this->makeFieldLabel();

        $this->currentErrorMessage = $this->current_rule['params']['message'] ??
        $this->validateErrorMessages[$messageType] ??
        $this->validateErrorMessages[$this->current_action] ??
            'Неописанный тип ошибки';

        $this->currentErrorMessage = str_replace('%field%', $this->currentFieldLabel, $this->currentErrorMessage);
        if (isset($this->current_rule['params']) && is_array($this->current_rule['params']))
        {
            foreach ($this->current_rule['params'] as $key => $value)
            {
                $this->currentErrorMessage = str_replace('%' . $key . '%', $value, $this->currentErrorMessage);
            }
        }

        $this->errors['result']                        = false;
        $this->errors['fields'][$this->current_path][] = $this->currentErrorMessage;

        return $this->currentErrorMessage;
    }

    /**
     * Функция определяет имя поля
     */
    public function makeFieldLabel()
    {
        $this->currentFieldLabel = $this->current_rule['params']['label'] ?? $this->labels[$this->current_path] ?? $this->current_path;
    }

    /**
     * Функция возвращает все пути полей правил
     *
     * @param $array
     */
    public static function getRulesAllPaths($array)
    {
        $ret = [];
        if (is_array($array))
        {
            foreach ($array as $k => $v)
            {
                if (is_array($v))
                {
                    $paths = self::getRulesAllPaths($v);
                    foreach ($paths as $path_item)
                    {
                        $ret[] = $k . '/' . $path_item;
                    }
                }
                else
                {
                    $ret[] = $v;
                }
            }

            return $ret;
        }
    }

    /**
     * Получаем значение поля по пути
     */
    public function getFieldValueByPath()
    {

        $path_array = explode('/', $this->current_path);

        $path_data_exists = true;
        $setFieldValue    = false;
        $fieldValue       = false;
        foreach ($path_array as $path_item)
        {

            if (!$setFieldValue)
            {
                if (!isset($this->data[$path_item]))
                {
                    $path_data_exists = false;
                }
                else
                {
                    $fieldValue = $this->data[$path_item];
                }
            }
            else
            {
                if (!isset($fieldValue[$path_item]))
                {
                    $path_data_exists = false;
                }
                else
                {
                    $fieldValue = $fieldValue[$path_item];
                }
            }

            if (!$path_data_exists)
            {
                break;
            }
            $setFieldValue = true;
        }

        //exit;

        return [
            'value'  => $fieldValue,
            'exists' => $path_data_exists,
        ];
    }

    /**
     * Устанавливаем значение поля по пути
     */
    public function setFieldValueByPath($value)
    {
        $path_array       = explode('/', $this->current_path);
        $path_data_exists = true;
        $data             = &$this->data;
        foreach ($path_array as $path_item)
        {
            if (isset($data[$path_item]))
            {
                $data = &$data[$path_item];
            }
            else
            {
                $path_data_exists = false;
            }
        }

        if ($path_data_exists)
        {
            $data = $value;
        }
    }

    /**
     * Проверяет на корректность регулярки
     *
     * @param  string   $regex string to validate
     * @return string
     */
    public static function isValidRegex($regex)
    {
        if (@preg_match($regex, null) !== false)
        {
            return true;
        }

        if (preg_last_error() != PREG_NO_ERROR)
        {
            return false;
        }

        return true;
    }

    /**
     * Проверяет на заполненность поля
     */
    public function validateActionRequired()
    {

        if (!$fieldValue['exists'])
        {
            $this->makeValidateErrorMessage();
        }

    }

    /**
     * Проверяем как строку
     */
    public function validateActionString()
    {
        if ($this->currentFieldValue['exists'])
        {
            if (!is_string($this->currentFieldValue['value']))
            {
                $this->makeValidateErrorMessage('String');

                return;
            }

            if (
                isset($this->current_rule['params']['min']) &&
                is_integer($this->current_rule['params']['min'])
            )
            {
                $min = $this->current_rule['params']['min'];
                if (mb_strlen($this->currentFieldValue['value']) < $min)
                {
                    $this->makeValidateErrorMessage('StringMinLength');

                    return;
                }
            }

            if (
                isset($this->current_rule['params']['max']) &&
                is_integer($this->current_rule['params']['max'])
            )
            {
                $max = $this->current_rule['params']['max'];
                if (mb_strlen($this->currentFieldValue['value']) > $max)
                {
                    $this->makeValidateErrorMessage('StringMaxLength');

                    return;
                }
            }
        }
    }

    /**
     * Проверяет параметр по регулярному выражению
     */
    public function validateActionRegexTest()
    {
        if ($this->currentFieldValue['exists'])
        {

            if (!is_string($this->currentFieldValue['value']))
            {
                $this->makeValidateErrorMessage('String');

                return;
            }

            if (
                (isset($this->current_rule['params']['regex'])) &&
                (strlen($this->current_rule['params']['regex']))
            )
            {
                $regex = $this->current_rule['params']['regex'];

                if (!self::isValidRegex($regex))
                {
                    $this->makeValidateErrorMessage('InvalidRegex');

                    return;
                }

                if (!preg_match($regex, $this->currentFieldValue['value']))
                {
                    $this->makeValidateErrorMessage();
                }
            }
        }

    }

    /**
     * Проверяет параметр по соответствию типу integer
     */
    public function validateActionInteger()
    {
        if ($this->currentFieldValue['exists'])
        {
            if (!is_integer($this->currentFieldValue['value']))
            {
                if (
                    (is_string($this->currentFieldValue['value'])) &&
                    (strval(intval($this->currentFieldValue['value'])) == $this->currentFieldValue['value'])
                )
                {
                    $this->setFieldValueByPath(intval($this->currentFieldValue['value']));
                    return;
                }

                $this->makeValidateErrorMessage();

                return;
            }
        }
    }

    /**
     * Проверяет параметр по соответствию типу Float
     */
    public function validateActionFloat()
    {
        if ($this->currentFieldValue['exists'])
        {
            if (!is_float($this->currentFieldValue['value']))
            {
                if (
                    (is_string($this->currentFieldValue['value'])) &&
                    (strval(floatval($this->currentFieldValue['value'])) == $this->currentFieldValue['value'])
                )
                {   
                    $this->setFieldValueByPath(floatval($this->currentFieldValue['value']));       
                    return;
                }

                $this->makeValidateErrorMessage();
            }
        }
    }

    /**
     * Проверяет является ли параметр номером телефона
     */
    public function validateActionPhone()
    {
        $fieldValue = $this->getFieldValueByPath();
        if ($fieldValue['exists'])
        {

            if (!is_string($this->currentFieldValue['value']))
            {
                $this->makeValidateErrorMessage('String');

                return;
            }

            $regex = "/^7\s\([\d]{3}\)\s[\d]{3}\-[\d]{2}\-[\d]{2}$/";
            if (!preg_match($regex, $this->currentFieldValue['value']))
            {
                $this->makeValidateErrorMessage();
                return;
            }

            $phone_digits = preg_replace('/\D+/', '', $this->currentFieldValue['value']);
            $this->setFieldValueByPath($phone_digits);  
        }
    }

}
