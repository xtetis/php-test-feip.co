<?php

return [
    [
        'fields' => [
            'employees' => [
                'employee' => [
                    '0' => [
                        'firstName',
                        'photo',
                        'not_exists_field',
                    ],
                ],
            ],
            'snils',
            'array2'    => ['idx'],

        ],
        'action' => 'Required',
        'params' => [],
    ],
    [
        'fields' => [
            'zzzz',
            'zzzz1',
        ],
        'action' => 'String',
        'params' => [
            'min' => 10,
            'max' => 20,
        ],
    ],
    [
        'fields' => [
            'array2' => ['idx', 'idx3'],
        ],
        'action' => 'RegexTest',
        'params' => [
            'regex' => 'asdasd',
        ],
    ],
    [
        'fields' => [
            'array2' => ['idx2'],
        ],
        'action' => 'RegexTest',
        'params' => [
            'regex'   => '/^[\d]{3}\-[\d]{3}\-[\d]{3}\s[\d]{2}$/',
            'label'   => 'Custom field label',
            'message' => 'Кастомное сообщение об ошибке для поля "%field%"',
        ],
    ],
    [
        'fields' => [
            'array2'             => ['idx4', 'idx5'],
            'integer_json_array' => [
                'idx1',
                'idx2',
                'idx3',
                'idx4',
            ],
        ],
        'action' => 'Integer',
        'params' => [],
    ],
    [
        'fields' => [
            'array2' => ['idx6', 'idx7', 'idx8'],
        ],
        'action' => 'Float',
        'params' => [],
    ],
    [
        'fields' => [
            'phones_array' => [
                'idx1',
                'idx2',
                'idx3',
                'idx4',
            ],
        ],
        'action' => 'Phone',
        'params' => [],
    ],
];
