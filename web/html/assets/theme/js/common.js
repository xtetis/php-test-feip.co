$(function() {
    $('#example_json').load('/examples/' + $('#example_id').val() + '/send.json');
    $('#example_rules').load('/?action=printr&ex_id=' + $('#example_id').val());


    $("#example_id").change(function() {
        $('#example_json').load('/examples/' + $('#example_id').val() + '/send.json');
        $('#example_rules').load('/?action=printr&ex_id=' + $('#example_id').val());
    });
});