<?php

require_once __DIR__ . '/inc/autoloader.php';

$action          = trim($_GET['action'] ?? '');
$filename_action = __DIR__ . '/actions/' . $action . '.php';
$filename_action = file_exists($filename_action) ? $filename_action : __DIR__.'/actions/index.php';
require $filename_action;
