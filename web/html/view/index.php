<!doctype html>
<html lang="en"
      class="h-100">

    <head>
        <link href="/assets/bootstrap4/css/bootstrap.min.css"
              rel="stylesheet">

    </head>

    <body class="d-flex flex-column h-100">

        <main role="main"
              class="flex-shrink-0">
            <div class="container-fluid">
                <h1 class="mt-5">Тестовая задача</h1>
                <p class="lead">Укажите JSON c данными для валидации</p>
                <form method="post"
                      action="/"
                      target="iframe_validate">
                    <div class="form-group">
                        <label>Тип данных</label>
                        <select name="example_id"
                                class="form-control" 
                                id="example_id">
                            <?php foreach (glob(__DIR__ . '/../examples/*') as $filename): ?>
                            <option value="<?=basename(realpath($filename))?>">Пример <?=basename(realpath($filename))?>
                            </option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>JSON для валидации</label>
                        <textarea class="form-control"
                                  name="json"
                                  id="example_json"
                                  rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Правила валидации</label>
                        <textarea class="form-control"
                                  readonly="readonly"
                                  id="example_rules"
                                  rows="5"></textarea>
                    </div>
                    <button type="submit"
                            class="btn btn-primary mb-2">Отправить</button>

                </form>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="iframe_validate"
                            class="embed-responsive-item"
                            name="iframe_validate"
                            frameborder="0"></iframe>
                </div>
            </div>
        </main>
        <script src="/assets/jquery/jquery-3.4.1.min.js"></script>
        <script src="/assets/bootstrap4/js/bootstrap.min.js"></script>
        <script src="assets/theme/js/common.js"></script>
    </body>
</html>
