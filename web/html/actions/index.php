<?php

$json       = trim($_POST['json'] ?? '');
$example_id = intval($_POST['example_id'] ?? 0);
if (!empty($_POST))
{
    if ((!strlen($json)) || (!$example_id))
    {
        die('Заполните поля');
    }

    $filename_rules = __DIR__ . '/../examples/' . $example_id . '/rules.php';
    if (!file_exists($filename_rules))
    {
        die('Некорректный пример');
    }
    $rules = require $filename_rules;   
    

    /**
     * Выполняем валидацию данных
     */
    $validator = new jsonValidator($json, $rules);
    $validator->validate();
    $response['errors'] = $validator->errors;
    $response['data'] = $validator->data;

    echo '<pre>' . Common::arrayToJson($response) . '</pre>';
    exit;
}

require __DIR__ . '/../view/index.php';
