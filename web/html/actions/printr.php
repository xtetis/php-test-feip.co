<?php

$example_id = intval($_GET['ex_id'] ?? '');
$filename   = __DIR__ . '/../examples/' . $example_id . '/rules.php';
if (file_exists($filename))
{
    $rules = require $filename;
}

print_r($rules);
